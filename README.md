Fillit is the second mandatory project at 42. The purpose is to make the smallest possible "square" (which can contain holes) 
with a given list of tetriminos, but the disposition must be as such that it returns the first possible solution when placing
them recursively from the top left.

Run make, an executable called fillit should compile directly.

Usage: ./fillit [file]
