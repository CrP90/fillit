/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spurice <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/17 23:35:56 by spurice           #+#    #+#             */
/*   Updated: 2017/12/17 23:35:57 by spurice          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *chr_list, const char *segment)
{
	char *a;
	char *b;

	if (*segment == '\0')
		return ((char *)chr_list);
	while (*chr_list)
	{
		a = (char *)chr_list;
		b = (char *)segment;
		while (*b && *a == *b)
		{
			a++;
			b++;
		}
		if (*b == '\0')
			return ((char *)chr_list);
		chr_list++;
	}
	return (NULL);
}
