/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spurice <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/17 23:35:29 by spurice           #+#    #+#             */
/*   Updated: 2017/12/17 23:35:30 by spurice          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *chr_list, const char *segment, size_t n)
{
	size_t i;
	size_t j;

	if (*segment == '\0')
		return ((char *)chr_list);
	i = 0;
	while (chr_list[i] && i < n)
	{
		j = 0;
		while (chr_list[i + j] && segment[j]
				&& chr_list[i + j] == segment[j]
				&& i + j < n)
			j++;
		if (segment[j] == '\0')
			return ((char *)(chr_list + i));
		i++;
	}
	return (NULL);
}
