/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spurice <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/17 20:47:04 by spurice           #+#    #+#             */
/*   Updated: 2017/12/17 20:47:05 by spurice          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *str)
{
	long long	nbr;
	char		isnegative;

	while (ft_iswhitespace(*str))
		str++;
	isnegative = (*str == '-');
	if (*str == '-' || *str == '+')
		str++;
	nbr = 0;
	while (ft_isdigit(*str))
	{
		nbr = nbr * 10 + (*str - '0');
		str++;
		if (nbr < 0 && !isnegative)
			return (-1);
		else if (nbr < 0 && isnegative)
			return (0);
	}
	return (isnegative ? -nbr : nbr);
}
